<?php if ($title): ?>
<h2><?php print $title; ?></h2>
<?php endif; ?>

<?php foreach ($portal_sets_content as $set_key=>$content): ?>

<div id="portal-set-<?php print $set_key; ?>">
  <?php print $content; ?>
</div>

<?php endforeach; ?>