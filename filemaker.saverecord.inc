<?php

// Turn on output buffering so that we can set Location: HTTP Header later on
//ob_start();
//
require_once 'filemaker.common.inc';

function filemaker_saverecord_callback($fmode) {
  // Get FileMaker object
  $fm_db          = $fmode->db_name;
  $fm_user        = $fmode->db_username;
  $fm_pass        = $fmode->db_password;
  $layout_name    = $fmode->layout_name;
  $fm             = _filemaker_load_fmapi($fm_db, $fm_user, $fm_pass);
  
}
/*
// Get POST vars
$recid = $_POST['recid'];
$field_name = $_POST['field-name'];
$field_value = $_POST['field-value'];

// Field names we expect as keys in $_POST[]
$keys = array(
    'Tasks::Job Code'
    );
//print_r($_POST);
// utility function to set field values from posted data
function setFieldData($record)
{
  // declare $keys as a global variable
  global $keys;
  // loop over each field value and append to array
  $result = array();
  foreach ($keys as $fieldname) {
    $value = null;
    // workaround PHP's insistence that spaces in
    // form variables be replaced by "_"
    if (!strpos($fieldname, " ")) {
      $value = $_POST[$fieldname];
    } else {
      $value = $_POST[str_replace(" ", "_", $fieldname)];
    }
    echo '<h1>VALUE = '. print $value .'</h1>';
    if (strlen($value) > 0) {
      $record->setField($fieldname, $value);
    } elseif (strlen($record->getField($fieldname)) > 0) {
      $record->setField($fieldname, null);
    }
  }
  return $result;
}
// declare $rec
$rec = null;

// check to see that user didn't hit 'cancel' button
if (!array_key_exists('cancel', $_POST)) {
  // Check for recid parameter which determines if this is a create new or edit
  if (array_key_exists('recid', $_POST)) {
    $rec = $fm->getRecordById('*Tasks', $_POST['recid']);
  } else {
    //$rec =& $fm->createRecord('Job_List', $values);
    echo '<h1>Cannot create a new record.</h1>';
  }
  if (FileMaker::isError($result)) {
      echo 'Record addition failed: (' . $result->getCode() . ') ' . $result->getMessage() . "\n";
      exit;
  }
  //print_r($rec);
  // set field data from form data
  //setFieldData($rec);
  // commit record to database
  $rec->setField('Job Code',$_POST['Tasks::Job_Code']);
  $result = $rec->commit();
  
  if (FileMaker::isError($result)) {
      echo 'Record addition failed: (' . $result->getCode() . ') ' . $result->getMessage() . "\n";
      exit;
  }
}

// set Location: HTTP header to force redirect
header("Location: /node/2");

// End output buffering and flush output
ob_end_flush();*/