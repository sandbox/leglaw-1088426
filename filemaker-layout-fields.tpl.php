<div id="layout-content">
  <?php foreach ($layout_fields_display as $field): ?>
  <div>
    <span class="fm-field-label"><?php print $field->field_label; ?>:</span>
    <span class="fm-field-data"><?php print $field->data; ?></span>
  </div>
  <?php  endforeach; ?>
</div>