<?php
// $Id$
//
/**
 * @file
 * Functions for handling detail view and access
 */

require_once 'filemaker.common.inc';

/**
 * Shows a detail view of a record within a FileMaker database.
 * 
 * @param Object $fmode
 *   Filemaker node object
 * @param int $rid
 *   The FileMaker ID of a specific FileMaker record.
 * @return string $output
 */
function filemaker_detail_page_view($fmode, $rid) {
  // Get FileMaker object
  $fm_db = $fmode->db_name;
  $fmuid = $fmode->fmuid;
  $layout_name = $fmode->layout_name;
  $fm = _filemaker_load_fmapi($fm_db, $fmuid);

  // Get display field and saved search data
  $layout_fields = unserialize($fmode->display_fields);
  $portal_fields = unserialize($fmode->related_display_fields);
  $search_params  = unserialize($fmode->search_parameters);
  
  $record = _filemaker_get_record($fm, $layout_name, (string)$rid);
  $related_sets = _filemaker_get_portal_sets($fm, $layout_name, $record);

  // Get display types and load any CSS or JS files
  $display_types = unserialize($fmode->display_types);
  foreach ($display_types as $module_name => $type) {
    if ($type['active']) {
      module_invoke($module_name, 'filemaker_prepare_display');
    }
  }

  $layout_fields_display = array();
  foreach ($layout_fields as $i => $field) {
//  	if ($field['display_chbx']) {
			$module = $display_types[$field['detail_display_type']]['module'];
      $data = module_invoke($module, 'filemaker_process_fields', $field, $record);
      $data = theme($field['detail_display_type'], $data, $field, $record, $fmode);
      $key = str_replace(' ', '_', strtolower($field['display_name']));
      $layout_fields_display[$key]->field_label = $field['display_name'];
      $layout_fields_display[$key]->data = $layout_fields[$i]['data'] = $data;
      if (in_array($field['display_name'], array('Street Address', 'City', 'State'))) {
        $add[$field['display_name']] = $data;
      }
//    }
  }
  $layout_content = theme('filemaker_layout_fields', $fmode, $rid, $layout_fields_display);

  $portal_sets_content = array();
  if ($portal_fields) {
    foreach ($portal_fields as $set_name => $fields) {
      $set_key = str_replace(' ', '_', strtolower($set_name));
      $portal_rows = array();
      $headers = array();
      foreach ($fields as $field) {
        if ($field['display_chbx']) {
          $headers[] = $field['display_name'];
        }
      }
      foreach ($related_sets[$set_name] as $i => $related_record) {
        foreach ($fields as $i => $field) {
//          if ($field['display_chbx']) {
            $field_key = str_replace(' ', '_', strtolower($field['display_name']));
            $module = $display_types[$field['list_display_type']]['module'];
            $data = module_invoke($module, 'filemaker_process_fields', $field, $related_record, $fmode, $rid, $i);
            $data = theme($field['list_display_type'], $data, $field, $related_record, $fmode);
            $portal_rows[$i][$field_key]->label = $field['display_name'];
            $portal_rows[$i][$field_key]->data = $portal_fields[$set_name][$i]['data'] = $data;
//          }
        }
      }
      if ($portal_rows) {
        $portal_sets_content[$set_key] = theme('filemaker_portal_set', $fmode, $rid, $set_name, $set_key, $headers, $portal_rows);
      }
    }
  }
  $additional_content = theme('filemaker_additional_output', $fmode, $rid, $layout_fields, $portal_fields);
  $output = theme('filemaker_record_detail_page', $fmode, $rid, $layout_content, $portal_sets_content, $additional_content);
  return $output;
}

function filemaker_record_create($fmode) {
  // Get FileMaker object
  $fmid = $fmode->fmid;
  $fm_db = $fmode->db_name;
  $fmuid = $fmode->fmuid;
  $page_title = $fmode->page_title;
  $layout_name = $fmode->layout_name;
  $fm = _filemaker_load_fmapi($fm_db, $fmuid);
  // Get display fields
  $layout_fields = unserialize($fmode->display_fields);

  return drupal_get_form('filemaker_record_modification_form', 'create', $fm, $fmode, $page_title, $layout_name, $layout_fields);
}

function filemaker_record_edit($fmode, $rid) {
  // Get FileMaker object
  $fmid = $fmode->fmid;
  $fm_db = $fmode->db_name;
  $fmuid = $fmode->fmuid;
  $page_title = $fmode->page_title;
  $layout_name = $fmode->layout_name;
  $fm = _filemaker_load_fmapi($fm_db, $fmuid);
  $record = _filemaker_get_record($fm, $layout_name, (string)$rid);
  // Get display fields
  $layout_fields = unserialize($fmode->display_fields);

  return drupal_get_form('filemaker_record_modification_form', 'edit', $fm, $fmode, $page_title, $layout_name, $layout_fields, $rid, $record);
}

/**
 * TODO: Finish this for record editing
 * @param <type> $form_state
 * @param <type> $mode
 * @param <type> $fm
 * @param <type> $fmode
 * @param <type> $page_title
 * @param <type> $layout_name
 * @param <type> $layout_fields
 * @param <type> $record
 * @return string
 */
function filemaker_record_modification_form(&$form_state, $mode, $fm, $fmode, $page_title, $layout_name, $layout_fields, $rid = NULL, $record = NULL) {
  // If a record was passed to this form then we're editing an existing record. Populate defaults.
  $defaults = array();
  if ($record) {
    foreach ($layout_fields as $i => $field) {
      $layout_fields[$i]['default'] = $record->getField($field['fm_field_name']);
      $defaults[$field['fm_field_name']] = $record->getField($field['fm_field_name']);
      if (in_array($field['edit_display_type'], array(FM_EDIT_CHECKBOX_SET, FM_EDIT_SELECTLIST))) {
        $defaults[$field['fm_field_name']] = _filemaker_process_checkboxes_list($record->getField($field['fm_field_name']));
        $layout_fields[$i]['default'] = _filemaker_process_checkboxes_list($record->getField($field['fm_field_name']));
      }
    }
  }
//  dpm($defaults, 'defaults');
//  dpm($layout_fields, 'layout fields');
  
  $form = array('fields' => array());
  $form['fmode'] = array(
    '#type' => 'value',
    '#value' => $fmode,
  );
  $form['page_title'] = array(
    '#type' => 'value',
    '#value' => $page_title,
  );
  if (isset($rid)) {
    $form['rid'] = array(
      '#type' => 'value',
      '#value' => $rid,
    );
  }

  $form['fields']['#tree'] = TRUE;
  foreach ($layout_fields as $field) {
    // Replace spaces with underscores
    $key = str_replace(' ', '_', strtolower($field['display_name']));
    // Now strip out invalid chars
    $key = str_replace(array('?', '#'), '', $key);
    $form['fields'][$key]['fm_field'] = array(
      '#type'     => 'value',
      '#value'    => $field,
    );

    $form['fields'][$key]['input']['#title'] = $field['display_name'];
    $form['fields'][$key]['input']['#required'] = FALSE;
    $form['fields'][$key]['input']['#id'] = str_replace(' ', '-', $field['fm_field_name']);

    if ($field['edit_display_type'] == FM_EDIT_TEXTFIELD ||
        $field['edit_display_type'] == FM_EDIT_TEXTAREA) {
      $type = ($field['edit_display_type'] == FM_EDIT_TEXTFIELD) ? 'textfield' : 'textarea';
      $form['fields'][$key]['input']['#type'] = $type;
      $form['fields'][$key]['input']['#size'] = 20;
      $form['fields'][$key]['input']['#default_value'] = isset($field['default']) ? $field['default'] : NULL;
    }
    else {
      // Compute value list options for radios, checkboxes, and select options
      $value_lists = _filemaker_get_value_lists($fm, $layout_name);
      $option_values = array_values($value_lists[$field['value_lists']]);
      $options = array();
      foreach ($option_values as $value) {
        $options[$value] = t(trim($value));
      }
      $form['fields'][$key]['input']['#options'] = $options;

      switch ($field['edit_display_type']) {
        case FM_EDIT_RADIO:
          $form['fields'][$key]['input']['#type'] = 'radios';
          $form['fields'][$key]['input']['#default_value'] = isset($field['default']) ? $field['default'] : NULL;
          break;
        case FM_EDIT_CHECKBOX:
          $form['fields'][$key]['input']['#type'] = 'checkbox';
          $form['fields'][$key]['input']['#default_value'] = isset($field['default']) ? $field['default'] : NULL;
          break;
        case FM_EDIT_CHECKBOX_SET:
          $form['fields'][$key]['input']['#type'] = 'checkboxes';
          if (isset($field['default'])) {
            $form['fields'][$key]['input']['#default_value'] = array_values($field['default']);
          }
          break;
        case FM_EDIT_SELECT:
          $form['fields'][$key]['input']['#type'] = 'select';
          $form['fields'][$key]['input']['#default_value'] = isset($field['default']) ? $field['default'] : NULL;
          break;
        case FM_EDIT_SELECTLIST:
          $form['fields'][$key]['input']['#type'] = 'select';
          $form['fields'][$key]['input']['#multiple'] = TRUE;
          $form['fields'][$key]['input']['#default_value'] = isset($field['default']) ? array_keys($field['default']) : NULL;
          break;
      }
    }
  }

  $form['submit'] = array(
    '#type'     => 'submit',
    '#value'    => t('Submit'),
  );

  return $form;

}

function filemaker_record_modification_form_validate($form, &$form_state) {

}

function filemaker_record_modification_form_submit($form, &$form_state) {
  $vals = $form_state['values'];
  $result = _filemaker_record_modify($vals['fmode'], $vals['fields'], $form_state['values']['rid']);
}

function template_preprocess_filemaker_portal_set(&$vars) {
  $vars['title'] = $vars['set_name'];
}

function template_preprocess_filemaker_record_detail_page(&$vars) {
  $vars['title'] = 'Details';

  $fmode = $vars['fmode'];
  $rid = $vars['rid'];
  $vars['template_files'][] = 'filemaker-record-detail-page--'. $fmode->url_path;
  $vars['template_files'][] = 'filemaker-record-detail-page--'. $fmode->url_path .'-'. $fmode->fmid;
  $vars['template_files'][] = 'filemaker-record-detail-page--'. $fmode->url_path .'-'. $fmode->fmid .'-'. $rid;
}

function theme_filemaker_additional_output() {
  // This exists to allow themes to construct output based on data from multiple fields.
  // Default is to do nothing extra.
  return '';
}