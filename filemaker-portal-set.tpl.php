<?php if ($title): ?>
<h3>Portal &mdash; <?php print $title; ?></h3>
<?php endif; ?>

<table id="portal-set-<?php print $set_key; ?>">
  <thead>
    <tr>
      <?php foreach ($headers as $header): ?>
      <th><?php print $header; ?></th>
      <?php endforeach; ?>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($portal_rows as $i=>$row): ?>
    <tr class="portal-row <?php print $i % 2 == 0 ? 'odd' : 'even'; ?>">
      <?php foreach ($row as $field_key=>$field): ?>
      <td><?php print $field->data; ?></td>
      <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>