<?php 
// $Id$
//
/**
 * @file
 * Common functions and includes for FileMaker module.
 */

// FileMaker field types
define('FILEMAKER_FIELD_UNDETERMINED', 0);
define('FILEMAKER_FIELD_TEXT', 1);
define('FILEMAKER_FIELD_NUMBER', 2);
define('FILEMAKER_FIELD_DATE', 3);
define('FILEMAKER_FIELD_TIME', 4);
define('FILEMAKER_FIELD_TIMESTAMP', 5);
define('FILEMAKER_FIELD_CONTAINER', 6);

// Numeric search options
define('FM_GREATER_THAN', 0);
define('FM_LESS_THAN', 1);
define('FM_EQUAL_TO', 2);
define('FM_BETWEEN', 3);
define('FM_GREATER_THAN_EQUAL_TO', 4);
define('FM_LESS_THAN_EQUAL_TO', 5);
define('FM_NOT_EQUAL_TO', 6);

// Include or omit
define('FM_CONTAIN', 7);
define('FM_OMIT', 8);

// The default "None" selection for value lists
define('FM_LIST_NONE', 1);

// Edit screen display options
define('FM_EDIT_NOTSET', 14);
define('FM_EDIT_TEXTFIELD', 15);
define('FM_EDIT_CHECKBOX', 16);
define('FM_EDIT_CHECKBOX_SET', 17);
define('FM_EDIT_RADIO', 18);
define('FM_EDIT_SELECT', 19);
define('FM_EDIT_SELECTLIST', 20);
define('FM_EDIT_TEXTAREA', 21);

/**
 * @param string $fm_db_name
 *  Name of FileMaker db
 * @param string $fm_dbuid
 *  The ID of The FileMaker user stored in filemaker_users
 * @return filemaker object
*/
function _filemaker_load_fmapi($fm_db_name = NULL, $fmuid = 0) {
  // Get path to local FileMaker API for PHP location and probe for file FileMaker.php and FileMaker dir
  global $base_path;
  $fm_api_location = variable_get('filemaker_api_location', 'sites/all/libraries/fmapi');
  $fm_api_path = '.'. $base_path . $fm_api_location .'/FileMaker.php'; // This works even if it's preceded by a double-slash
  // Test for presence of FileMaker API before executing require_once
  if (!file_exists($fm_api_path)) {
    drupal_set_message('FileMaker API not found.', 'error');
    drupal_goto('admin/settings/filemaker');
  }
  require_once $fm_api_path;

  $fm_server_url = variable_get('filemaker_server_url', 'http://localhost');
  if ($fmuid !== '0') {
    // Login with FileMaker user. Get FileMaker username as password
    $user_pass = _filemaker_get_user_pass($fmuid);
    $fm = new FileMaker($fm_db_name, $fm_server_url, $user_pass['username'], $user_pass['password']);
  }
  else {
    $fm = new FileMaker($fm_db_name, $fm_server_url);
  }
  
  return $fm;
}

function _filemaker_list_databases() {
  $dbs = _filemaker_load_fmapi()->listDatabases();
  $db_options = drupal_map_assoc($dbs);
  return $db_options;
}

function _filemaker_list_layouts($fm) {
  return $fm->listLayouts();
}

function _filemaker_get_additional_info($fm) {
  return array(
    'api_version' => $fm->getAPIVersion(),
    'scripts' => $fm->listScripts(),
    'properties' => $fm->getProperties(),
  );
}

/**
 * Helper function to get fields from a layout (not including related sets (portals))
 * @param FileMaker object $fm
 * @param string $layout_name
 * @return 0-index array 
 */

/**
 *
 * @param object $fm
 *  Instance of the FileMaker API Class
 * @param string $layout_name
 * @param object $record
 *  Instance of the FileMaker API Record class
 * @return array $record_fields
 *  All the fields belonging to the layout
 */
function _filemaker_get_fields($fm, $layout_name, $record=NULL) {
  static $fields;
  if (!empty($fields)) {
    return $fields;
  }
  elseif ($record) {
    return $record->getFields();
  }
  $findAnyRequest =& $fm->newFindAnyCommand($layout_name);
  $result = $findAnyRequest->execute();
  if (_filemaker_error($result)) {
    return FALSE;
  }
  $records = $result->getRecords();
  $fields = $records[0]->getFields();
  return $fields;
}

function _filemaker_containerdata($fmode) {
  // Get FileMaker object
  $fm_db          = $fmode->db_name;
  $fmuid          = $fmode->fmuid;
  $layout_name    = $fmode->layout_name;
  $fm             = _filemaker_load_fmapi($fm_db, $fmuid);
  
  $url = $_GET['url'];
  $type = $_GET['type'];
  switch ($type) {
    case 'jpg':
      header('Content-type: image/jpeg');
      break;
    case 'gif':
      header('Content-type: image/jpeg');
      break;
    case 'data':
      header('Content-type: application/octet-stream');
      header('Content-Transfer-Encoding: Binary');
      $fname = basename($url);
      header('Content-Disposition: attachment; filename="'. $fname .'"');
      
  }
  
  $matches = array();
  if (preg_match('/^Server\/Documents(.+)/', $url, $matches)) {
    echo $fm->getContainerData($matches[1]);
  }
  else {
    echo $fm->getContainerData($_GET['url']);
  }
}

/**
 * Helper function to get fields from a layout (not including related sets (portals))
 * @param FileMaker object $fm
 * @param string $layout_name
 * @return 0-index array 
 */
function _filemaker_get_field_data($fm, $layout_name) {
  $layout =& $fm->getLayout($layout_name);
  $get_fields =& $layout->getFields();
  
  return $get_fields;
}

function _filemaker_get_portal_fields($fm, $layout_name) {
  static $portal_fields;
  if (!empty($portal_fields)) {
    return $portal_fields;
  }
  $layout =& $fm->getLayout($layout_name);
//  $related_sets =& $layout->getRelatedSets();
  
  $portal_fields = array();
  foreach ($layout->getRelatedSets() as $set_name => $related_set) {
    $portal_fields[$set_name] = array();
    foreach ($related_set->getFields() as $field) {
      $portal_fields[$set_name][] = $field->getName();
    } 
  }
  
  return $portal_fields;
}

function _filemaker_get_portal_field_data($fm, $layout_name) {
  static $portal_field_data;
  if (!empty($portal_field_data)) {
    return $portal_field_data;
  }
  $layout =& $fm->getLayout($layout_name);
//  $related_sets =& $layout->getRelatedSets();
  
  $portal_field_data = array();
  foreach ($layout->getRelatedSets() as $set_name => $related_set) {
    $portal_field_data[$set_name] = $related_set->getFields();
  }
  
  return $portal_field_data;
}

function _filemaker_get_portal_sets($fm, $layout_name, $record) {
  // The only way I've seen in the API to get an array of all the sets.
  // There's no method that does the same.
  return $record->_impl->_relatedSets;
}

/**
 * Gets Value Lists from using FM layout object
 * @param $fm
 * @param $layout_name
 * @return
 *   multi-dimension array with list name as key and an array list as the value.
 */
function _filemaker_get_value_lists($fm, $fm_layout_name) {
  static $value_lists;
  if (!empty($value_lists)) {
    return $value_lists;
  }
  $layout = $fm->getLayout($fm_layout_name);
  if (_filemaker_error($layout)) {
    return FALSE;
  }

  $value_lists = $layout->getValueListsTwoFields();
  return $value_lists;
}

/**
 * This function does nothing because layouts of related sets returns the FileMaker_RelatedSet type,
 * which does not support extended data on FileMaker_Field types.
 * @param unknown_type $field_data
 */
//TODO: Fix this function somehow, or remove.
function _filemaker_get_related_value_lists($field_data) {
  $keys = array_keys($field_data);
  $related_layout =& $field_data[key($field_data)]->getLayout();
  // The function getValueListsTwoFields does not exist for related set fields.
  //$related_value_lists = $related_layout->getValueListsTwoFields();
  //return $related_value_lists;
}

/**
 * Sorts $display_fields and record's $fields by weight. Pass by reference.
 * @param array $display_fields
 * @param array $fields
 * @return NULL
 */
function _filemaker_weight_sort(&$display_fields, &$fields) {
  // First, get the $display_fields in the same order as $fields.
  $ids = array();
  foreach ($display_fields as $field) {
    $ids[] = $field['id'];
  }
  array_multisort($ids, $display_fields);
  
  // Now get the weights by which we can match sorting on both.
  $weights = array();
  foreach ($display_fields as $id => $field) {
    $weights[] = $display_fields[$id]['weight'];
  }
  $original = $weights;
  array_multisort($weights, $display_fields);
  array_multisort($original, $fields);
}

/**
 * Returns a default display_list and attempts to ascertain field type (num, text, date, container)
 * @param array $field_data
 *   array retrieved from _filemaker_get_field_data()
 * @return
 *   an array with order matching fields returned from _filemaker_get_fields()
 */
function _filemaker_get_field_defaults($fm, $fm_layout_name, $fields, $value_lists, $use_portal_data = FALSE) {
  if ($use_portal_data) {
    $field_data = $use_portal_data;
  }
  else {
    $field_data = _filemaker_get_field_data($fm, $fm_layout_name);
  }

  $display_fields = array();
  foreach ($fields as $i => $field) {
    $display_fields[$i]['display_chbx'] = 1;
    $display_fields[$i]['display_name'] = $field;
    $display_fields[$i]['fm_field_name'] = $field;
    $display_fields[$i]['weight'] = $i;
    $display_fields[$i]['id'] = $i;

    $valueList = $field_data[$field]->getValueList();
    if ($valueList && !_filemaker_error($valueList, FALSE)) {
      foreach ($value_lists as $list_name => $list) {
        $display_fields[$i]['value_lists'] = $field_data[$field]->_impl->_valueList;
      }
    } else {
      $display_fields[$i]['value_lists'] = FM_LIST_NONE;
    }
    
    $fieldType = $field_data[$field]->getResult();
    if ($fieldType == 'text') {
      $display_fields[$i]['field_type'] = FILEMAKER_FIELD_TEXT;
    } elseif ($fieldType == 'number') {
      $display_fields[$i]['field_type'] = FILEMAKER_FIELD_NUMBER;
    } elseif ($fieldType == 'date') {
      $display_fields[$i]['field_type'] = FILEMAKER_FIELD_DATE;
    } elseif ($fieldType == 'time') {
      $display_fields[$i]['field_type'] = FILEMAKER_FIELD_TIME;
    } elseif ($fieldType == 'timestamp') {
      $display_fields[$i]['field_type'] = FILEMAKER_FIELD_TIMESTAMP;
    } elseif ($fieldType == 'container') {
      $display_fields[$i]['field_type'] = FILEMAKER_FIELD_CONTAINER;
    }
    
    $styleType = $field_data[$field]->getStyleType();
    if (_filemaker_error($styleType, FALSE)) {
      $display_fields[$i]['list_display_type'] = 'filemaker_list_text';
      $display_fields[$i]['detail_display_type'] = 'filemaker_list_text';
      $display_fields[$i]['edit_display_type'] = FM_EDIT_TEXTFIELD;
    } elseif ($styleType == 'EDITTEXT') {
      $display_fields[$i]['list_display_type'] = ($i == 0) ? 'filemaker_list_link' : 'filemaker_list_text';
      $display_fields[$i]['detail_display_type'] = 'filemaker_list_text';
      $display_fields[$i]['edit_display_type'] = FM_EDIT_TEXTFIELD;
    } elseif ($styleType == 'SCROLLTEXT') {
      $display_fields[$i]['list_display_type'] = ($i == 0) ? 'filemaker_list_link' : 'filemaker_list_text';
      $display_fields[$i]['detail_display_type'] = 'filemaker_list_text';
      $display_fields[$i]['edit_display_type'] = FM_EDIT_TEXTFIELD;
    } elseif ($styleType == 'POPUPLIST') {
      $display_fields[$i]['list_display_type'] = 'filemaker_list_ul';
      $display_fields[$i]['detail_display_type'] = 'filemaker_list_ul';
      $display_fields[$i]['edit_display_type'] = FM_EDIT_SELECTLIST;
    } elseif ($styleType == 'POPUPMENU') {
      $display_fields[$i]['list_display_type'] = 'filemaker_list_ul';
      $display_fields[$i]['detail_display_type'] = 'filemaker_list_ul';
      $display_fields[$i]['edit_display_type'] = FM_EDIT_SELECT;
    } elseif ($styleType == 'CHECKBOX') {
      $display_fields[$i]['list_display_type'] = 'filemaker_list_ul';
      $display_fields[$i]['detail_display_type'] = 'filemaker_list_ul';
      $display_fields[$i]['edit_display_type'] = FM_EDIT_CHECKBOX_SET;
    } elseif ($styleType == 'RADIOBUTTONS') {
      $display_fields[$i]['list_display_type'] = 'filemaker_list_text';
      $display_fields[$i]['detail_display_type'] = 'filemaker_list_text';
      $display_fields[$i]['edit_display_type'] = FM_EDIT_RADIO;
    } elseif ($styleType == 'CALENDAR') {
      $display_fields[$i]['list_display_type'] = 'filemaker_list_text';
      $display_fields[$i]['detail_display_type'] = 'filemaker_list_text';
      $display_fields[$i]['edit_display_type'] = FM_EDIT_TEXTFIELD;
    }
  }
  
  return $display_fields;
}

/**
 * Deterines FileMaker data type for $data input
 * @param string $data
 * @return
 *   FileMaker field constant
 */
function _filemaker_get_input_type($data) {
  if (!$data) {
    return FILEMAKER_FIELD_UNDETERMINED; 
  }
  if (is_numeric($data)) {
    return FILEMAKER_FIELD_NUMBER;
  }
  if (preg_match('/^((\<|\>|=)?\d+)$/', $data)) {
    return FILEMAKER_FIELD_NUMBER;
  }
  if (preg_match('/^(\d+\.{3}\d+)$/', $data)) {
    return FILEMAKER_FIELD_NUMBER;
  }
  if (preg_match('/^(\/fmi\/xml\/cnt)/', $data)) {
    return FILEMAKER_FIELD_CONTAINER;
  }
  if (preg_match('/^((\<|\>|=)?\d{1,2}\/\d{1,2}\/\d{4})$/', $data)) {
    return FILEMAKER_FIELD_DATE;
  }
  if (preg_match('/^(\d{1,2}\/\d{1,2}\/\d{4}\.{3}\d{1,2}\/\d{1,2}\/\d{4})$/', $data)) {
    return FILEMAKER_FIELD_DATE;
  }
  if (preg_match('/^((\<|\>|=)?\d{1,2}:\d{1,2}:\d{1,2})$/', $data)) {
    return FILEMAKER_FIELD_TIME;
  }
  if (preg_match('/(\d{1,2}\/\d{1,2}\/\d{4})/', $data) && preg_match('/(\d{1,2}:\d{1,2}:\d{1,2})/', $data)) {
    return FILEMAKER_FIELD_TIMESTAMP;
  }
  # if nothing else, return text
  return FILEMAKER_FIELD_TEXT;
}

/**
 * Checks presence of data from value lists against field data in a record 
 * @param string $data
 *   Data from one field
 * @param array $value_lists
 *   Keyed array of list_name => (array)list_items
 * @return array $found
 *   An array of list_name => times_found. Count may be more than one if data appear in two value lists.
 */  
function _filemaker_value_list_check($data, $value_lists) {
  $found = array();
  foreach ($value_lists as $list_name => $list) {
    foreach ($list as $item) {
      $pattern = '/^('. $item .')$/';
      $match = preg_match($pattern, $data);
      if ($match) {
        if (isset($found[$list_name])) {
          $found[$list_name] += $match;
        }
        else {
          $found[$list_name] = $match;
        }
      }
    }
  }
  return $found;
}

function _filemaker_format_date_string($date) {
  $date_string = '';
  // TODO: future - allow for more date input methods.
  if (is_array($date)) {
    $date_string .= $date['month'] .'/'. $date['day'] .'/'. $date['year'];
  }
  return $date_string;
}

function filemaker_format_key_field($data) {
  return str_replace(' ', '_', strtolower($data));
}

/**
 * Tests result set for errors
 * Returns TRUE if error found.
 * @param Any FileMaker API object type $result
 */
function _filemaker_error($result, $display_error = TRUE) {
  if (FileMaker::isError($result)) {
    if ($display_error) {
      drupal_set_message($result->getMessage(), 'error');
    }
    return TRUE;
  }
  return FALSE;
}

function _filemaker_get_headers($fields, $display_fields, $fm_layout_name) {
  // Sanity check -- make sure keys in $display_fields match values in $fields. Perhaps send them back to the edit page.
  if (user_access('edit any FileMaker content') || (user_access('edit own FileMaker content')) || user_access('administer FileMaker content')) {
    $disp_keys = array();
    foreach ($display_fields as $field) {
      $disp_keys[] = $field['fm_field_name'];
    }
    sort($fields); sort($disp_keys);
    if ($disp_keys != $fields) {
      drupal_set_message('"'. $fm_layout_name .'" layout has changed. Please edit this page and save to update field listing.', 'warning');
    }
  }
  $headers = array();
  foreach ($display_fields as $field) {
    if ($field['display_chbx']) {
      $headers[] = array('data' => $field['display_name'] , 'field' => $field['fm_field_name']);
    }
  }
  $headers[0]['sort'] = 'desc';
  return $headers;
}
/**
 *
 * @param object $fm
 *  Instance of the FileMaker API class
 * @param string $layout_name
 * @param string $rid
 *  String representation of the record ID. String is required for FileMaker API
 * @return object $record
 *  Instance of the FileMaker API Record class
 */
function _filemaker_get_record($fm, $layout_name, $rid) {
  return $fm->getRecordById($layout_name, (string)$rid);
}

/**
 * Data from a checkbox set is returned as a \r or \r\n delimited string. This returns those values
 * as a more useful array.
 * @param string $cl_data
 *  The \r\n delimited checkbox data retrieved by a FileMaker object
 * @return array
 */
function _filemaker_process_checkboxes_list($cl_data) {
  return explode('||||', str_replace(array("\r\n", "\r", "\n"), '||||', $cl_data));
}

/**
 * TODO: Finish this implementation for record editing
 * @param <type> $fmode
 * @param <type> $field_values
 * @return <type>
 */
function _filemaker_record_modify($fmode, $field_values, $rid = NULL) {
  $fmid = $fmode->fmid;
  $fm_db = $fmode->db_name;
  $fmuid = $fmode->fmuid;
  $page_title = $fmode->page_title;
  $layout_name = $fmode->layout_name;
  $fm = _filemaker_load_fmapi($fmode->db_name, $fmode->fmuid);

  dpm($field_values, 'field values');
  if (isset($rid)) {
    $fmrecord = _filemaker_get_record($fm, $layout_name, (string)$rid);
  }
  else {
    $fmrecord =& $fm->newAddCommand($layout_name);
  }
  
  foreach ($field_values as $field_value) {
    if (!empty($field_value['input'])) {
      $fm_field_name = $field_value['fm_field']['fm_field_name'];
      $input = $field_value['input'];
      if (is_array($input)) {
        $val_string = '';
        foreach ($input as $key => $val) {
          if (empty($val)) {
            unset($input[$key]);
          }
        }
        $input = implode("\r", $input);
      }
      $fmrecord->setField($fm_field_name, $input);
    }
  }
  // If edit then commit(). Otherwise, execute()
  if (isset($rid)) {
    $result = $fmrecord->commit();
//    $rid = $fmrecord->getRecords()[0]
  }
  else {
    $result = $fmrecord->execute();
    $record = $result->getRecords();
    $rid = $record[0]->getRecordId();
  }
  return $result;
}

/**
 * Gets search result if $query_list is defined, all results otherwise.
 * @param FileMaker object $fm
 *   The FileMaker API object
 * @param string $fm_layout_name
 *   Name of layout in the filemaker db
 * @param array $query_list
 *   array of search parameters (see $form_state['storage']['search_parameters'])
 * @param int $record_skip
 *   Skips to a certain page
 * @param int $limit
 *   Max results per page
 * @param array $headers
 *   0-based key of headers for table for tablesorting
 * @return FileMaker object $result 
 *   FileMaker search result object
 */
function _filemaker_get_results($fm, $fm_layout_name, $headers, $query_list = NULL, $record_skip = 0, $limit = 10) {
  // Get sorting data to perform sort using FileMaker api. This also gets the current page argument.
  $ts = tablesort_init($headers);
  # get appropriate FileMaker sort constant
  $fm_sort = ($ts['sort'] == 'desc') ? FILEMAKER_SORT_DESCEND : FILEMAKER_SORT_ASCEND;
  
  if ($query_list) {
    $omit = array();
    $search_query = _filemaker_create_query_request($query_list, &$omit);
    
    $requests = array();
    foreach ($search_query as $i => $request_item) {
      $requests[$i] =& $fm->newFindRequest($fm_layout_name);
      foreach ($request_item as $field => $query) {
        if (is_array($query)) {
          // If is array, then simple search is attempting to search on field
          // whose field is defined in the search params in the content view.
          // Limit search scope by combining the query terms.
          if (stristr(trim($query[0], '*'), trim($query[1], '*'))) {
            $requests[$i]->addFindCriterion($field, $query[0]);
          }
          else {
            $requests[$i]->addFindCriterion($field, $query[0] . $query[1]);
          }
          /*
          foreach ($query as $value) {
            $requests[$i]->addFindCriterion($field, $value);
          }*/
        } else {
          $requests[$i]->addFindCriterion($field, $query);
        }
      }
    }
    
    if ($omit) {
      $i = count($requests);
      foreach ($omit as $omit_request) {
        foreach ($omit_request as $field => $query) {
          $requests[$i] =& $fm->newFindRequest($fm_layout_name);
          $requests[$i]->addFindCriterion($field, $query);
          $requests[$i]->setOmit(TRUE);
          $i++;
        }
      }
    }
  }
  
  if (isset($requests)) {
    $findCommand =& $fm->newCompoundFindCommand($fm_layout_name);
    foreach ($requests as $i => $request) {
      $findCommand->add($i+1, $request);
    }
  }
  else { // If no search queries given, list all records.
    $findCommand =& $fm->newFindAllCommand($fm_layout_name);
  }
  $findCommand->setRange($record_skip, $limit);
  $findCommand->addSortRule($ts['sql'], 1, $fm_sort);
  $result = $findCommand->execute();
  return $result;
}

function _filemaker_get_page_list() {
  $pages = array();
  
  $sql = "SELECT fmid, page_title, url_path FROM {filemaker_pages}";
  $result = db_query($sql);
  while ($page_row = db_fetch_object($result)) {
    $pages[] = $page_row;
  }
  
  return $pages;
}

function _filemaker_create_query_request($s, &$omit) {
  $query_layout = array();
  $simple_search = array();
  $omit = array();
  $omit_ptn = '/(_OMIT)$/';
  // Create array in format 'fieldname' => array(query1, query2, query3, ...)
  // Omits go into a separate array to be appended to the query array at the very end.
  foreach ($s as $search) {
    foreach ($search as $field => $query) {
      if (preg_match($omit_ptn, $query)) {
        $query = preg_replace($omit_ptn, '', $query);
        $omit[] = array($field => $query);
      }
      elseif (isset($search['simple_search'])) {
        $simple_search[$field] = $query;
      }
      else {
        if (isset($query_layout[$field])) {
          array_push($query_layout[$field], $query);
        }
        else {
          $query_layout[$field] = array($query);
        }
      }
    }
  }
  
  arsort($query_layout);
  
  $s_size = array();
  $size_fields_arr = array();
  foreach ($query_layout as $field => $arr) {
    $count = count($arr);
    $s_size[$field] = $count;
    if (isset($size_fields_arr[$count])) {
      array_push($size_fields_arr[$count], $field);
    }
    else {
      $size_fields_arr[$count] = array($field);
    }
  }
  
  $query = array();
  $first = array_slice($size_fields_arr, 0, 1, TRUE);
  $size_fields_arr = array_slice($size_fields_arr, 1, count($size_fields_arr), TRUE);
  foreach ($first as $size => $fields) {
    for ($i = 0; $i < $size; $i++) {
      foreach ($fields as $field) {
        $query[$i][$field] = $query_layout[$field][$i];
      }
    }
  }
  
  if ($size_fields_arr) {
    foreach ($size_fields_arr as $size => $fields) {
      $cur_size = count($query);
      $new_size = $size * $cur_size;
      $qp = $query;
      for ($i = 1; $i < $size; $i++) {
        foreach ($qp as $search) {
          array_push($query, $search);
        }
      }
      $j = -1;
      for ($i = 0; $i < $new_size; $i++) {
        $j++;
        foreach ($fields as $field) {
          $query[$i][$field] = $query_layout[$field][$j];
        }
        if ($j == $s_size[$field] - 1) {
          $j = -1;
        }
      }
    }
  }
  
  if ($simple_search) {
    unset($simple_search['simple_search']);
    // If content creator defined no view parameters, return let users search on all display fields
    if (!$query_layout) {
      foreach ($simple_search as $field => $search) {
        $query[] = array($field => $search);
      }
    }
    else {
      // If simple searching on a field already defined in this view's search parameters,
      // add the simple search to each query to create an AND search.
      // First take a snapshot of the current query before it's modified.
      // This will be useful when adding queries on fields not yet defined by the view.
      $query_snapshot = $query;
      $found_key = FALSE;
      foreach (array_keys($simple_search) as $key) {
        if (array_key_exists($key, $query_layout)) {
          $found_key = TRUE;
          for ($i = 0; $i < count($query); $i++) {
            $query[$i][$key] = array($query[$i][$key], $simple_search[$key]);
          }
          unset($simple_search[$key]);
        }
      }
      // If not searching on field defined in content view, all records will be displayed unless we reset the query.
      if (!$found_key) {
        $query = array();
      }
      
      // Loop over the rest of the simple searches, copy and modify the snapshot, and push it onto the query
      $loop_counter = count($query_snapshot);
      foreach ($simple_search as $field => $search) {
        $query_addition = $query_snapshot;
        for ($i = 0; $i < $loop_counter; $i++) {
          $query_addition[$i][$field] = $search;
        }
        $query = array_merge($query, $query_addition);
      }
    }
  }
  
  return $query;
}

/**
 * Class that offers encryption of FileMaker user passwords stored in the MySQL db
 */
class filemaker_cryptastic {
   /** Encryption Procedure
    *
    *  @param mixed msg message/data
    *  @param string k encryption key
    *  @param boolean base64 base64 encode result
    *
    *  @return string iv+ciphertext+mac or
    * boolean false on error
    */
  public function encrypt( $msg, $k, $base64 = FALSE ) {
    # open cipher module (do not change cipher/mode)
    if ( ! $td = mcrypt_module_open('rijndael-256', '', 'ctr', '') )
        return FALSE;

    $msg = serialize($msg);                         # serialize
    $iv = mcrypt_create_iv(32, MCRYPT_RAND);        # create iv

    if ( mcrypt_generic_init($td, $k, $iv) !== 0 )  # initialize buffers
        return FALSE;

    $msg = mcrypt_generic($td, $msg);               # encrypt
    $msg = $iv . $msg;                              # prepend iv
    $mac = $this->pbkdf2($msg, $k, 1000, 32);       # create mac
    $msg .= $mac;                                   # append mac

    mcrypt_generic_deinit($td);                     # clear buffers
    mcrypt_module_close($td);                       # close cipher module

    if ( $base64 ) $msg = base64_encode($msg);      # base64 encode?

    return $msg;                                    # return iv+ciphertext+mac
  }

  /** Decryption Procedure
   *
   *  @param string msg output from encrypt()
   *  @param string k encryption key
   *  @param boolean base64 base64 decode msg
   *
   *  @return string original message/data or
   * boolean false on error
   */
  public function decrypt( $msg, $k, $base64 = FALSE ) {

    if ( $base64 ) $msg = base64_decode($msg);          # base64 decode?

    # open cipher module (do not change cipher/mode)
    if ( ! $td = mcrypt_module_open('rijndael-256', '', 'ctr', '') )
      return FALSE;

    $iv = substr($msg, 0, 32);                          # extract iv
    $mo = strlen($msg) - 32;                            # mac offset
    $em = substr($msg, $mo);                            # extract mac
    $msg = substr($msg, 32, strlen($msg)-64);           # extract ciphertext
    $mac = $this->pbkdf2($iv . $msg, $k, 1000, 32);     # create mac

    if ( $em !== $mac )                                 # authenticate mac
      return FALSE;

    if ( mcrypt_generic_init($td, $k, $iv) !== 0 )      # initialize buffers
      return FALSE;

    $msg = mdecrypt_generic($td, $msg);                 # decrypt
    $msg = unserialize($msg);                           # unserialize

    mcrypt_generic_deinit($td);                         # clear buffers
    mcrypt_module_close($td);                           # close cipher module

    return $msg;                                        # return original msg
  }

  /** PBKDF2 Implementation (as described in RFC 2898);
   *
   *  @param string p password
   *  @param string s salt
   *  @param int c iteration count (use 1000 or higher)
   *  @param int kl derived key length
   *  @param string a hash algorithm
   *
   *  @return string derived key
   */
  public function pbkdf2( $p, $s, $c, $kl, $a = 'sha256' ) {

    $hl = strlen(hash($a, null, TRUE)); # Hash length
    $kb = ceil($kl / $hl);              # Key blocks to compute
    $dk = '';                           # Derived key

    # Create key
    for ( $block = 1; $block <= $kb; $block ++ ) {
      # Initial hash for this block
      $ib = $b = hash_hmac($a, $s . pack('N', $block), $p, TRUE);
      # Perform block iterations
      for ( $i = 1; $i < $c; $i ++ ) {
        # XOR each iterate
        $ib ^= ($b = hash_hmac($a, $b, $p, TRUE));
      }
      $dk .= $ib; # Append iterated block
    }

   # Return derived key of correct length
   return substr($dk, 0, $kl);
  }
}

function _filemaker_get_security_key() {
  $sql = "SELECT fm_web_pass, fm_web_salt FROM {filemaker_settings} WHERE id=1";
  $result = db_fetch_array(db_query($sql));
  if (!$result) {
    drupal_set_message('Could not retrieve security settings.', 'error');
    return;
  }
  $fm_crypt = new filemaker_cryptastic();
  return $fm_crypt->pbkdf2($result['fm_web_pass'], $result['fm_web_salt'], 1000, 32);
}

/**
 * Get FileMaker username and password from fmuid.
 * @param int $fmuid
 *  The FileMaker user id
 * @return array $user_pass
 *  An array containing elements 'username' and 'password'
 */
function _filemaker_get_user_pass($fmuid) {
  $sql = "SELECT username, password FROM {filemaker_users} WHERE fmuid=%d";
  $result = db_query($sql, $fmuid);
  if (!$result) {
    drupal_set_message(t('Error retriving FileMaker user from db.'), 'error');
    return FALSE;
  }
  $user_pass = db_fetch_array($result);

  // Decrypt user password
  $fm_crypt = new filemaker_cryptastic();
  $key = _filemaker_get_security_key();
  $password_decrypted = $fm_crypt->decrypt($user_pass['password'], $key);
  $user_pass['password'] = $password_decrypted;
  return $user_pass;
}

function _filemaker_get_user_select_options($fmuid = NULL) {
  $fm_user_options = array();
  // Add option for Guest access
  $fm_user_options[0] = '[Guest]';
  $sql = "SELECT fmuid, username FROM {filemaker_users}";
  $result = db_query($sql);
  while ($fm_user = db_fetch_object($result)) {
    $fm_user_options[$fm_user->fmuid] = $fm_user->username;
  }
  return $fm_user_options;
}
