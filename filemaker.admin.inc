<?php
// $Id$
//
/**
 * @file
 * Menu callbacks for filemaker module.
 */

require_once 'filemaker.common.inc';

/**
 * Admin settings page.
 * 
 * @ingroup forms
 * @see system_settings_form().
 */

/************************************
 * 
 * FileMaker server settings
 *
 ************************************/
function filemaker_admin_api_server_settings() {
  /**
   * Server Settings
   */
  $form['server_settings'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('Server Settings'),
    '#weight'         => 0,
    '#collapsible'    => FALSE,
  );
  $form['server_settings']['filemaker_api_location'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Location of FileMaker API for PHP'),
    '#description'    => t('Local path to location of FileMaker API for PHP. <br />For FileMaker Server on Mac OS X, the zip file containing the API can be found in /Library/FileMaker Server/Web Publishing/FM_API_for_PHP_Standalone.zip'),
    '#default_value'  => variable_get('filemaker_api_location', 'sites/all/libraries/fmapi'),
    '#size'           => 100
  );
  $form['server_settings']['filemaker_server_url'] = array(
    '#type'           => 'textfield',
    '#title'          => t('URL of the FileMaker Server'),
    '#description'    => t('If a non-standard web port has been configured, enter this value as in http://localhost:88'),
    '#default_value'  => variable_get('filemaker_server_url', 'http://localhost'),
    '#size'           => 100
  );

  $form['fm_security'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('FileMaker user security'),
    '#weight'         => 1,
    '#collapsible'    => FALSE,
  );
  $form['fm_security']['password'] = array(
    '#type'           => 'password',
    '#title'          => t('FM Web Password'),
  );
  $form['fm_security']['salt'] = array(
    '#type'           => 'password',
    '#title'          => t('FM Web Salt'),
  );
  
  return system_settings_form($form);
}

/************************************
 *
 * View, add, edit, remove FileMaker users
 *
 ************************************/
function filemaker_admin_list_users() {
  $output = '';

  if (!$content) {
    $output .= '<h2>No content</h2>';
    $output .= '<p>No FileMaker content has been added. Go to the <a href="/filemaker" title="Add FileMaker content">FileMaker content</a> page to get your database online.</p>';
  }

  // Get list of all currently defined FileMaker users
  $filemaker_users = array();
  $sql = "SELECT fmuid, username, drupal_role FROM {filemaker_users}";
  $result = db_query($sql);
  while ($filemaker_user = db_fetch_object($result)) {
    $fmuid = $filemaker_user->fmuid;
    $filemaker_users[] = array(
      'fmuid'       => $filemaker_user->fmuid,
      'username'    => $filemaker_user->username,
      'drupal_role' => $filemaker_user->drupal_role,
    );
  }

  // Display link to create users if none
  if (!$filemaker_users) {
    $output .= '<h2>No Users</h2>';
    $output .= '<p>No FileMaker users have been configured. Click on <a href="/admin/settings/filemaker/users/add" title="Add a FileMaker user">Add</a> to set up a FileMaker user from one the FileMaker content you\'ve set up.</p>';
  }
  else {
    $drupal_roles = _filemaker_get_drupal_roles();
    $header = array('FileMaker user', 'Role assigned', 'Operations');
    $rows = array();
    foreach ($filemaker_users as $row => $filemaker_user) {
      $drupal_role_name = $drupal_roles['id_rolename'][$filemaker_user['drupal_role']];
      $rows[$row][] = $filemaker_user['username'];
      $rows[$row][] = $drupal_role_name;
      $operations = l('edit', 'admin/settings/filemaker/users/'. $filemaker_user['fmuid'] .'/edit') . ', ' .
                    l('remove', 'admin/settings/filemaker/users/'. $filemaker_user['fmuid'] .'/remove');
      $rows[$row][] = $operations;
    }
    $output .= '<h2>FileMaker users</h2>';
    $output .= theme('table', $header, $rows);
  }
  
  return $output;
}

function filemaker_admin_view_user($fm_user) {
  $output = '';
  $output .= '<h2>No Statistics</h2>';
  $output .= '<p>There are no stats for user'. $fm_user->username .'.</p>';
  return $output;
}

function filemaker_admin_edit_user($fm_user) {
  return drupal_get_form('filemaker_admin_add_user', $fm_user);
}

function filemaker_admin_remove_user(&$form_state, $fm_user) {
  $form = array();
  $form['#redirect'] = 'admin/settings/filemaker/users';
  $form['title'] = array(
    '#type'         => 'markup',
    '#value'        => '<h2>Are you sure you wish to remove user <em>'. $fm_user->username .'</em>?</h2>',
  );
  $form['fmuid'] = array(
    '#type'         => 'value',
    '#value'        => $fm_user->fmuid,
  );
  return confirm_form($form, '',
                      'admin/settings/filemaker/users', t('This action cannot be undone.'),
                      t('Remove'), t('Cancel'));
}

function filemaker_admin_remove_user_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $fmuid = $form_state['values']['fmuid'];
    $sql = "DELETE FROM {filemaker_users} WHERE fmuid=%d";
    $result = db_query($sql, $fmuid);
    if (!$result) {
      drupal_set_message('Error removing user', 'error');
    }
    else {
      drupal_set_message('FileMaker user has been removed.');
    }
  }
}

function filemaker_admin_add_user(&$form_state, $fm_user = NULL) {
  // First see if the password and salt have been set. If not, redirect user to user security settings page
  $sql = "SELECT fm_web_pass, fm_web_salt FROM {filemaker_settings} WHERE id=1";
  $result = db_fetch_array(db_query($sql));
  if (!$result) {
    drupal_set_message('You must define your security passwords before adding your FileMaker users.');
    drupal_goto('admin/settings/filemaker/security');
  }

  // Get a list of all Drupal roles
  $drupal_roles = _filemaker_get_drupal_roles();
  $rolename_id = $drupal_roles['rolename_id'];

  // Get a list of all Drupal roles that are already assigned to FileMaker users
  // and remove those from the array of drupal_roles. Only one role per user.
  $sql = "SELECT drupal_role FROM {filemaker_users}";
  $result = db_query($sql);
  $assigned_roles = array();
  while ($value = db_fetch_array($result)) {
    // Unset if creating a new user.
    // If this is a user edit, don't unset if it is the role currently assigned to the user
    if (!$fm_user || $fm_user->drupal_role != $value['drupal_role']) {
      $assigned_roles[] = $value['drupal_role'];
    }
  }
  $role_options = array_keys(array_diff($rolename_id, $assigned_roles));
  array_unshift($role_options, '-- Select a role --');


  // Init form array
  $form = array();
  $form['#redirect'] = 'admin/settings/filemaker/users';
  $form_state['storage']['drupal_roles'] = $drupal_roles;
  $form_state['storage']['role_options'] = $role_options;

  // Compute defaults if this is a user edit
  $defaults = array();
  if ($fm_user) {
    $defaults['filemaker_user'] = $fm_user->username;

    $assigned_rolename = $drupal_roles['id_rolename'][$fm_user->drupal_role];
    $defaults['drupal_role'] = array_search($assigned_rolename, $role_options);

//    $defaults['drupal_role'] = $fm_user->drupal_role;
    $defaults['password_description'] = t('Leave blank to keep current password.');
    $defaults['password_required'] = FALSE;

    $form['fmuid'] = array(
      '#type'     => 'value',
      '#value'    => $fm_user->fmuid,
    );
  }
  else {
    $defaults['password_description'] = t('Password for the FileMaker user.');
    $defaults['password_required'] = TRUE;
  }

  // If there are no more Drupal roles to choose from, let user know.
  if (!$fm_user && count($drupal_roles) == 1) {
    $form['no_available_drupal_roles'] = array(
      '#type'     => 'markup',
      '#value'    => t('<h2>No Drupal Roles</h2><p>All current Drupal roles have already been assigned to other FileMaker users. Please create more Drupal roles if you wish to create more FileMaker users.</p>'),
    );
  }
  // Ok, there are Drupal roles available, let user create a FileMaker user.
  else {
    $form['filemaker_user'] = array(
      '#type'           => 'textfield',
      '#title'          => t('FileMaker username'),
      '#description'    => t('Note: This user must have fmphp privileges to the database.'),
      '#required'       => TRUE,
      '#default_value'  => $defaults['filemaker_user'],
      '#size'           => 40,
    );
    $form['filemaker_password'] = array(
      '#type'           => 'password',
      '#title'          => t('FileMaker password'),
      '#description'    => $defaults['password_description'],
      '#required'       => $defaults['password_required'],
      '#size'           => 40,
    );
    $form['drupal_role'] = array(
      '#type'           => 'select',
      '#title'          => t('Drupal role'),
      '#description'    => t('Select a Drupal role to that will be associated to this FileMaker user.'),
      '#required'       => TRUE,
      '#options'        => $role_options,
      '#default_value'  => $defaults['drupal_role'],
    );

    $form['submit'] = array(
      '#type'           => 'submit',
      '#value'          => t('Submit'),
    );
  }

  return $form;
}

function filemaker_admin_add_user_validate($form, &$form_state) {
  if ($form_state['values']['drupal_role'] == 0) {
    form_set_error('drupal_role', 'Select a Drupal role');
  }
}

function filemaker_admin_add_user_submit($form, &$form_state) {
  $drupal_roles = $form_state['storage']['drupal_roles'];
  $role_options = $form_state['storage']['role_options'];

  // New user or edit?
  $fmuid = $form_state['values']['fmuid'] ? $form_state['values']['fmuid'] : NULL;

  $selected_option = $form_state['values']['drupal_role'];
  $selected_role_id = $drupal_roles['rolename_id'][$role_options[$selected_option]];

  // Get security settings for encryption
  $key = _filemaker_get_security_key();
  $cryptastic = new filemaker_cryptastic();

  $username = $form_state['values']['filemaker_user'];
  $password = $cryptastic->encrypt($form_state['values']['filemaker_password'], $key);

  if ($fmuid) {
    if (strlen($form_state['values']['filemaker_password']) > 0) {
      dpm('password detected');
      $sql = "UPDATE {filemaker_users} "
            ."SET username='%s', password='%s', drupal_role=%d "
            ."WHERE fmuid=%d";
      $result = db_query($sql, $username, $password, $selected_role_id, $fmuid);
    }
    else {
      $sql = "UPDATE {filemaker_users} "
            ."SET username='%s', drupal_role=%d "
            ."WHERE fmuid=%d";
      $result = db_query($sql, $username, $selected_role_id, $fmuid);
    }
    if (!result) {
      drupal_set_message(t('Error updating user.'), 'error');
    }
    $message_suffix = t('updated');
  }
  else {
    $sql = "INSERT INTO {filemaker_users} "
          ."(username, password, drupal_role) "
          ."VALUES ('%s', '%s', %d)";
    $result = db_query($sql, $username, $password, $selected_role_id);
    $message_suffix = t('created');
  }
  if ($result) {
    drupal_set_message(t('FileMaker user <em>'. $username .'</em> has been '. $message_suffix .'.'));
  }
  else {
    drupal_set_message(t('Error creating user.'), 'error');
  }
  unset($form_state['storage']);
}

/************************************
 *
 * FileMaker security settings
 *
 ************************************/
function filemaker_admin_user_security(&$form_state) {
  // This adds the password javascript help. Same as what's used in user.module
  _filemaker_password_dynamic_validation();

  // Check if there's a password already set
  $sql = "SELECT fm_web_pass FROM {filemaker_settings} WHERE id=1";
  $current_password = db_result(db_query($sql));

  $warning_msg = <<<EOT
<div id="filemaker-admin-password-warning">
  <div class="title">WARNING:</div>
  <p>Resetting your web and/or salt passwords will require you to delete and recreate all of your FileMaker users.</p>
</div>
EOT;
  $form = array();
  $form['#redirect'] = 'admin/settings/filemaker/users/add';

  $form['fm_security'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('FileMaker user security'),
    '#weight'         => 0,
    '#collapsible'    => FALSE,
  );
  if ($current_password) {
    $form['fm_security']['current_password'] = array(
      '#type'           => 'password',
      '#title'          => t('Current password'),
      '#description'    => t('It is required that you confirm your current web password before you can change the web password or the password salt.'),
      '#required'       => TRUE,
      '#weight'         => 0,
    );
    $form['fm_security']['warning'] = array(
      '#type' => 'markup',
      '#value' => $warning_msg,
      '#weith' => 5,
    );
  }
  $form['fm_security']['fm_web_password'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('FileMaker web password'),
    '#weight'         => 10,
    '#collapsible'    => FALSE,
  );
  $form['fm_security']['fm_web_password']['password'] = array(
    '#type'           => 'password_confirm',
    '#required'       => TRUE,
  );
  $form['fm_security']['fm_salt_password'] = array(
    '#type'           => 'fieldset',
    '#title'          => t('FileMaker password salt'),
    '#weight'         => 20,
    '#collapsible'    => FALSE,
  );
  $form['fm_security']['fm_salt_password']['salt'] = array(
    '#type'           => 'password_confirm',
    '#required'       => TRUE,
  );
  
  $form['fm_security']['submit'] = array(
    '#type'           => 'submit',
    '#value'          => t('Submit'),
    '#weight'         => 30,
  );
  
  return $form;
}

function filemaker_admin_user_security_validate($form, &$form_state) {
  // Get password currently inside database, if there is one.
  $sql = "SELECT fm_web_pass FROM {filemaker_settings} WHERE id=1";
  $current_password = db_result(db_query($sql));
  if (!$current_password) {
    // Since there's no password to replace, validation passes.
    return;
  }
  
  // Next, compare to what was entered
  $current_password_input = md5($form_state['values']['current_password']);
  if ($current_password_input != $current_password) {
    form_set_error('current_password', 'Password does not match that which is currently in the database.', 'error');
  }
}

function filemaker_admin_user_security_submit($form, &$form_state) {
  $password = md5($form_state['values']['password']);
  $salt = md5($form_state['values']['salt']);

  // Only one row in the filemaker_settings table
  $sql = "DELETE FROM {filemaker_settings} WHERE id=1";
  $result = db_query($sql);
  if (!$result) {
    drupal_set_message('DB Error when deleting', 'error');
  }
  $sql = "INSERT INTO {filemaker_settings} "
        ."(id, fm_web_pass, fm_web_salt) VALUES (1,'%s','%s')";
  $result = db_query($sql, $password, $salt);
  if (!$result) {
    drupal_set_message('DB Error while inserting', 'error');
  }
  else {
    drupal_set_message('Password and password salt has been set.');
  }
  unset($form_state['storage']);
}

/**
 * Add javascript and string translations for dynamic password validation
 * (strength and confirmation checking).
 *
 * This is an internal function that makes it easier to manage the translation
 * strings that need to be passed to the javascript code.
 */
function _filemaker_password_dynamic_validation() {
  static $complete = FALSE;
  global $user;
  // Only need to do once per page.
  if (!$complete) {
    drupal_add_js(drupal_get_path('module', 'user') .'/user.js', 'module');

    drupal_add_js(array(
      'password' => array(
        'strengthTitle' => t('Password strength:'),
        'lowStrength' => t('Low'),
        'mediumStrength' => t('Medium'),
        'highStrength' => t('High'),
        'tooShort' => t('It is recommended to choose a password that contains at least six characters. It should include numbers, punctuation, and both upper and lowercase letters.'),
        'needsMoreVariation' => t('The password does not include enough variation to be secure. Try:'),
        'addLetters' => t('Adding both upper and lowercase letters.'),
        'addNumbers' => t('Adding numbers.'),
        'addPunctuation' => t('Adding punctuation.'),
        'sameAsUsername' => t('It is recommended to choose a password different from the username.'),
        'confirmSuccess' => t('Yes'),
        'confirmFailure' => t('No'),
        'confirmTitle' => t('Passwords match:'),
        'username' => (isset($user->name) ? $user->name : ''))),
      'setting');
    $complete = TRUE;
  }
}

function _filemaker_get_drupal_roles() {
  $drupal_rolename_id = array();
  $drupal_id_rolename = array();
  $sql = "SELECT * FROM {role}";
  $result = db_query($sql);
  while ($role = db_fetch_object($result)) {
    $drupal_rolename_id[$role->name] = (string)$role->rid;
    $drupal_id_rolename[$role->rid] = $role->name;
  }
  return array('id_rolename' => $drupal_id_rolename, 'rolename_id' => $drupal_rolename_id);
}


/************************************
 *
 * FileMaker content removal handling
 *
 ************************************/
function filemaker_page_remove(&$form_state, $fmode) {
  $fmid = $fmode->fmid;
  $page_title = $fmode->page_title;
  $url_path = $fmode->url_path;

  $form['filemaker'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);

  $form['filemaker']['title'] = array(
    '#type'         => 'markup',
    '#value'        => '<h2>Are you sure you wish to remove this FileMaker content view?</h2>
                        <br />'. $page_title .'<br />',
  );
  $form['filemaker']['fmid'] = array(
    '#type'         => 'value',
    '#value'        => $fmid,
  );
  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');
  return confirm_form($form, '',
                      'filemaker/'. $fmid, t('This action cannot be undone.'),
                      t('Remove'), t('Cancel'));
}

function filemaker_page_remove_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $sql = "DELETE FROM {filemaker} WHERE fmid=%d";
    $result = db_query($sql, (int)$form_state['values']['filemaker']['fmid']);
    $sql = "DELETE FROM {filemaker_pages} WHERE fmid=%d";
    $result = db_query($sql, (int)$form_state['values']['filemaker']['fmid']);
    if ($result) {
      drupal_set_message(t('The content view has been removed.'));
    }
  }
  $form_state['redirect'] = 'filemaker';
  return;
}

