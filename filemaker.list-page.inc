<?php 
// $Id$

/**
 * @file
 * FileMaker pages.
 */

require_once 'filemaker.common.inc';

/*****************************************************************************
 * URL callback handling
 *****************************************************************************/

/**
 * Summary page showing all created FileMaker content pages.
 * @global string $base_root
 * @global string $base_path
 * @return string HTML output
 */
function filemaker_summary_page() {
  global $base_root, $base_path;

  $module_path = drupal_get_path('module', 'filemaker');
  drupal_add_css($module_path .'/css/filemaker.css');
  $output = '';
  
  $filemaker_page_list = _filemaker_get_page_list();
  
  $output .= '<div id="filemaker-page-listing"><ul>'; 
  foreach ($filemaker_page_list as $page) {
    $list_page_path = $base_root . $base_path .'filemaker/page/'. $page->fmid;
    $search_page_path = $base_root . $base_path .'filemaker/search/'. $page->fmid;
    $output .= '<li>';
    $output .= '<span class="filemaker-list-page">'. l($page->page_title, $list_page_path) .'</span>';
    $output .= '<span class="filemaker-search-page">'. l('Search page', $search_page_path) .'</span>';
    $output .= '</li>';
  }
  $output .= '</ul></div>'; # /filemaker-page-listing
  return $output;
}

function filemaker_search_page($fmode) {
  // Get FileMaker object
  $fmid = $fmode->fmid;
  $fm_db = $fmode->db_name;
  $fmuid = $fmode->fmuid;
  $page_title = $fmode->page_title;
  $layout_name = $fmode->layout_name;
  $fm = _filemaker_load_fmapi($fm_db, $fmuid);
  // Get display fields
  $layout_fields = unserialize($fmode->display_fields);
  
  return drupal_get_form('filemaker_search_page_form', $fm, $fmode, $fmid, $layout_name, $layout_fields, $page_title);
}

function filemaker_list_page_view($fmode) {
  // Get FileMaker object
  $fm_db          = $fmode->db_name;
  $fmuid          = $fmode->fmuid;
  $layout_name    = $fmode->layout_name;
  $fm             = _filemaker_load_fmapi($fm_db, $fmuid);
  // Get display field and saved search data
  $layout_fields = unserialize($fmode->display_fields);
  $search_params  = unserialize($fmode->search_parameters);

  $additional_info = _filemaker_get_additional_info($fm);

  // Merge user input search with existing search parameters
  $search_params = filemaker_process_page_search($_GET, $search_params, $layout_fields);

  // Get fields from active fm db connection, compare it with display_fields stored in db to check for changes
  $fields = _filemaker_get_fields($fm, $layout_name);
  // This function also checks to make sure $fields and $layout_fields are in sync
  $headers = _filemaker_get_headers($fields, $layout_fields, $layout_name);

  // Get current display field
  $limit = 10; # static for now
  // Test whether user typed in new page or clicked link
  if (isset($_POST['page'])) {
    $post_page = (int)$_POST['page'];
    if ($post_page < 1) { $post_page = 1; }
    $post_pager_max = (int)$_POST['pager_max'];
    $page = ($post_page > $post_pager_max) ? $post_pager_max - 1 : $post_page - 1;
  } else {
    $page = isset($_GET['page']) ? (int)$_GET['page'] : 0; # not going to care about unexpected values -- strings resolve to int 0
  }
  $record_skip = $page * $limit;


  // Get results
  $result = _filemaker_get_results($fm, $layout_name, $headers, $search_params, $record_skip, $limit);
  if (_filemaker_error($result)) {
    $output .= '<h2>No records found</h2>';
    $records = NULL;
    $foundCount = 0;
  }
  else {
    $records = $result->getRecords();
    $foundCount = $result->getFoundSetCount();
  }

  // Set pager globals to set current and total pages
  global $pager_page_array, $pager_total; # get pager globals
  $pager_page_array = array($page);
  $pager_total = array( ceil($foundCount / $limit) );

  // Output data
  drupal_add_css(drupal_get_path('module', 'filemaker') . '/css/filemaker.css');
  $output = '';
  $output .= drupal_get_form('filemaker_simple_search', $fmode->fmid);
  $output .= theme('filemaker_total_records', $foundCount);
  if ($records) {
    $output .= theme('filemaker_field_table', $records, $headers, $layout_fields, $limit, $fmode);
  }

  // Add CSS
  $module = drupal_get_path('module', 'filemaker');
  drupal_add_css($module .'/css/filemaker.css');
  return $output;
}

/*****************************************************************************
 * Functions that define forms presented on pages
 *****************************************************************************/

/**
 * Displays single textfield, google-type search.
 * @param array $form_state
 * @param int $fmid
 *  The FM content page id
 * @return array $form
 */
function filemaker_simple_search(&$form_state, $fmid) {
  $form = array();

  $form['fmid'] = array(
    '#type' => 'value',
    '#value' => $fmid,
  );

  $form['simple_search'] = array(
    '#type'         => 'textfield',
    '#default_value'=> isset($_GET['search']) ? $_GET['search'] : NULL,
    '#size'         => 30,
    '#required'     => FALSE,
  );
  $form['submit'] = array(
    '#type'   => 'submit',
    '#value'  => t('Search'),
  );
  $form['reset'] = array(
    '#type'   => 'submit',
    '#value'  => t('Reset'),
    '#attributes' => array('onclick' => "$('#edit-simple-search').val('')"),
  );
  return $form;
}

function filemaker_simple_search_submit($form, &$form_state) {
  $val = $form_state['values'];
  $url = 'filemaker/page/'. $val['fmid'];
  if (!empty($val['simple_search'])) {
    $search = 'search='. $val['simple_search'];
  }
  $form_state['redirect'] = array($url, $search);
}

/**
 *
 * @param array $form_state
 * @param Object<FileMaker> $fm
 *  An instance of the FileMaker ojbect
 * @param array $fmid
 *  The ID of a particular FM content page
 * @param array $layout_fields
 *  Field settings for that FM page
 * @return array $form
 */
function filemaker_search_page_form(&$form_state, $fm, $fmode, $fmid, $layout_name, $layout_fields, $page_title) {
  $form = array('fields' => array());
  $form['fmode'] = array(
    '#type' => 'value',
    '#value' => $fmode,
  );
  $form['fmid'] = array(
    '#type' => 'value',
    '#value' => $fmid,
  );
  $form['page_title'] = array(
    '#type' => 'value',
    '#value' => $page_title,
  );
  
  $form['fields']['#tree'] = TRUE;
  foreach ($layout_fields as $field) {
    if ($field['display_chbx']) {
      // Replace spaces with underscores
      $key = str_replace(' ', '_', strtolower($field['display_name']));
      // Now strip out invalid chars
      $key = str_replace(array('?', '#'), '', $key);
      $form['fields'][$key]['fm_field'] = array(
        '#type'     => 'value',
        '#value'    => $field,
      );

      $form['fields'][$key]['input']['#title'] = $field['display_name'];
      $form['fields'][$key]['input']['#required'] = FALSE;
      $form['fields'][$key]['input']['#id'] = str_replace(' ', '-', $field['fm_field_name']);

      if ($field['edit_display_type'] == FM_EDIT_TEXTFIELD) {
        $form['fields'][$key]['input']['#type'] = 'textfield';
        $form['fields'][$key]['input']['#size'] = 20;
      }
      else {
        // Compute value list options for radios, checkboxes, and select options
        $value_lists = _filemaker_get_value_lists($fm, $layout_name);
        $option_values = array_values($value_lists[$field['value_lists']]);
        $options = array();
        foreach ($option_values as $value) {
          $options[$value] = t(trim($value));
        }
        $form['fields'][$key]['input']['#options'] = $options;

        switch ($field['edit_display_type']) {
          case FM_EDIT_RADIO:
            $form['fields'][$key]['input']['#type'] = 'radios';
            break;
          case FM_EDIT_CHECKBOX:
            $form['fields'][$key]['input']['#type'] = 'checkbox';
            break;
          case FM_EDIT_CHECKBOX_SET:
            $form['fields'][$key]['input']['#type'] = 'checkboxes';
            break;
          case FM_EDIT_SELECT:
            $form['fields'][$key]['input']['#type'] = 'select';
            break;
          case FM_EDIT_SELECTLIST:
            $form['fields'][$key]['input']['#type'] = 'select';
            $form['fields'][$key]['input']['#multiple'] = TRUE;
            break;
        }
      }
    }
  }

  $form['submit'] = array(
    '#type'     => 'submit',
    '#value'    => t('Submit'),
  );
  
  return $form;
}

function template_preprocess_filemaker_search_page_form(&$vars) {
  $vars['title'] = t('Search') .' '. $vars['form']['page_title']['#value'];
  $form = $vars['form'];

  $vars['search_fields'] = array();
  foreach(element_children($form['fields']) as $key) {
    $search_field =& $vars['form']['fields'][$key];
    $vars['search_fields'][$key]->field_data = $search_field['fm_field']['#value'];
    $vars['search_fields'][$key]->input_field = drupal_render($search_field['input']);
  }

  $vars['form_submit'] = drupal_render($vars['form']);

  $fmode = $vars['form']['fmode']['#value'];
  $vars['template_files'][] = 'filemaker-search-page-form--'. $fmode->url_path;
  $vars['template_files'][] = 'filemaker-search-page-form--'. $fmode->url_path .'-'. $fmode->fmid;
  dpm($vars, 'vars');
}

function filemaker_search_page_form_validate($form, &$form_state) {
}

function filemaker_search_page_form_submit($form, &$form_state) {
  $val = $form_state['values'];

  $query = array();
  foreach ($val['fields'] as $field) {
    if ($field['input']) {
      $query[$field['fm_field']['display_name']] = $field['input'];
    }
  }

  $url = 'filemaker/page/'. $val['fmid'];
  $query_str = '';
  foreach ($query as $field => $q) {
    if (is_array($q)) {
      $query_str .= $field .'=';
      foreach ($q as $item) {
        if ($item) {
          $query_str .= $item .' (OR) ';
        }
      }
      $query_str = trim($query_str, ' (OR) ') .'&';
    }
    else {
      $query_str .= $field .'='. $q .'&';
    }
  }
  $query_str = trim($query_str, '&');

  $form_state['redirect'] = array($url, $query_str);
}

/*****************************************************************************
 * Data processing functions
 *****************************************************************************/

/**
 * Processes google-type searches and field-specific searches.
 * For field-specific searches, this function tests $query for fields existing
 * in this FM page and merges search parameters into existing $search_params var.
 * @param array $query
 *  The $_GET function arguments array.
 * @param array $search_params
 *  Search parameters saved to the FM content.
 * @param array $layout_fields
 *  Field settings.
 * @return array $search_params
 *  The merged array
 */
function filemaker_process_page_search($query, $search_params, $layout_fields) {
  // Get rid of arguments not relevant to FM search processing.
  unset($query['q'], $query['sort'], $query['order']);

  // Search type is only one or the other and simple search takes precendence.
  if (isset($query['search'])) {
    $simple_search = $query['search'];
    $simple_search_type = _filemaker_get_input_type($simple_search);
    $simple_search_query = array();
    $search_values = array_values($search_params);
    $search_restraint = array();

    foreach ($layout_fields as $field) {
      if ($field['display_chbx']) {
        if ($field['field_type'] == FILEMAKER_FIELD_TEXT &&
            $simple_search_type == FILEMAKER_FIELD_TEXT) {
          $simple_search_query[] = array(
            $field['fm_field_name'] => '*'.$simple_search.'*',
            'simple_search'         => TRUE,
          );
        }
        elseif ($field['field_type'] == FILEMAKER_FIELD_NUMBER &&
                  $simple_search_type == FILEMAKER_FIELD_NUMBER) {
          $simple_search_query[] = array(
            $field['fm_field_name'] => $simple_search,
            'simple_search'         => TRUE,
          );
        }
        elseif ($field['field_type'] == FILEMAKER_FIELD_DATE &&
                  $simple_search_type == FILEMAKER_FIELD_DATE) {
          $simple_search_query[] = array(
            $field['fm_field_name'] => $simple_search,
            'simple_search'         => TRUE,
          );
        }
      }
    }
    $search_params = array_merge($search_params, $simple_search_query);
  }
  else {
    $args_fields = array_keys($query);
    $search_fields = array();
    // This searches the layout fields whose display_chbx is true and creates
    // a new array to be merged with $search_params. simple_search is set to
    // TRUE if field entry from search page was input for a field for which
    // search parameters are set for the FM content.
    $key = 0;
    foreach ($layout_fields as $i => $field) {
      if ($field['display_chbx']) {
        $f_name = str_replace(' ', '_', $field['display_name']);
        if (isset($query[$f_name])) {
          if (stripos($query[$f_name], ' (OR) ')) {
            $q_arr = explode(' (OR) ', $query[$f_name]);
            foreach ($q_arr as $q) {
              $search_fields[$key][$field['fm_field_name']] = '*'. $q .'*';
//              $search_fields[$key]['simple_search'] = TRUE;
              $key++;
            }
          }
          else {
            $search_fields[$key][$field['fm_field_name']] = '*'. $query[$f_name] .'*';
            foreach ($search_params as $search_item) {
              if (isset($search_item[$field['fm_field_name']])) {
                $search_fields[$key]['simple_search'] = TRUE;
              }
            }
            $key++;
          }
        }
      }
    }
    $search_params = array_merge($search_params, $search_fields);
  }

  return $search_params;
}

/*****************************************************************************
 * THEME functions
 *****************************************************************************/

function theme_filemaker_total_records($foundCount) {
  $output = '<h2>Found <span id="filemaker_total_records">'. $foundCount .'</span> records</h2>';
  return $output;
}

function theme_filemaker_field_table($records, $headers, $layout_fields, $limit, $fmode) {
  // Get display types and allow modules to load any CSS or JS files
  $display_types = unserialize($fmode->display_types);
  foreach ($display_types as $module_name => $type) {
    if ($type['active']) {
      module_invoke($module_name, 'filemaker_prepare_display');
    }
  }
  
  $output = '';
  $output .= '<div id="filemaker-field-table">';
  $rows = array();
  foreach ($records as $row => $record) {
    foreach ($layout_fields as $field) {
      $i = 0;
      if ($field['display_chbx']) {
        $module = $display_types[$field['list_display_type']]['module'];
        $data = module_invoke($module, 'filemaker_process_fields', $field, $record, $fmode);
        $rows[$row][] = theme($field['list_display_type'], $data, $field, $record, $fmode);
      }
    }
  }
  $output .= theme('table', $headers, $rows);
  $output .= '</div>';
  
  // Add a pager
  $output .= '<div id="filemaker_pager">';
  $output .= theme('filemaker_pager', NULL, $limit);
  $output .= '</div>';
  return $output;
}

function theme_filemaker_display_field($field, $record, $fmode) {
  $output = '';
  
  switch ($field['list_display_type']) {
    case 'filemaker_list_text':
      $output .= theme('filemaker_list_text', $field, $record);
      break;
    case 'filemaker_list_link':
      $output .= theme('filemaker_list_link', $field, $record, $fmode);
      break;
    case 'filemaker_list_ul':
      $output .= theme('filemaker_list_ul', $field, $record);
      break;
    case 'filemaker_list_ol':
      $output .= theme('filemaker_list_ol', $field, $record);
      break;
  }
  
  return $output;
}

/**
 * Mostly copied from theme_pager -- just added the page select input.
 */
function theme_filemaker_pager($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 9) {
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current =  $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', (isset($tags[0]) ? $tags[0] : t('« First')), $limit, $element, $parameters);
  $li_previous = theme('pager_previous', (isset($tags[1]) ? $tags[1] : t('‹ Previous')), $limit, $element, 1, $parameters);
  $li_next = theme('pager_next', (isset($tags[3]) ? $tags[3] : t('Next ›')), $limit, $element, 1, $parameters);
  $li_last = theme('pager_last', (isset($tags[4]) ? $tags[4] : t('Last »')), $limit, $element, $parameters);

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => 'pager-first',
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => 'pager-previous',
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => 'pager-ellipsis',
          'data' => '<span>&hellip;</span>',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'data' => theme('pager_previous', $i, $limit, $element, ($pager_current - $i), $parameters),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => 'pager-current',
            'data' => '<span>'. $i .'</span>',
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'data' => theme('pager_next', $i, $limit, $element, ($i - $pager_current), $parameters),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => 'pager-ellipsis',
          'data' => '<span>&hellip;</span>',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'data' => $li_last,
      );
    }

    // Prepare the pager output
    $output = '<div class="pager clearfix">'."\n";
    $output .= '  <div class="pager-inner">'."\n";
    
    // Produce form for use to jump to page
    $output .= '    <form name="page-selector" method="post">'; 
    $output .= '      <span id="page-select" style="float: left; margin-top: 5px;">Page ';
    $output .= '      <input type="text" name="page" value="'. $pager_current .'" size="3" maxlength="3" /> of '. $pager_max .'</span>';
    $output .= '      <input type="hidden" name="pager_max" value="'. $pager_max .' />"';
    $output .= '    </form>';
    
    $output .=     theme('item_list', $items, NULL, 'ul', array('class' => 'pager')) ."\n";
    $output .= '  </div>'."\n";
    $output .= '</div>'."\n";

    return $output;
  }
}
