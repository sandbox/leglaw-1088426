<?php if ($title): ?>
<h1><?php print $title; ?></h1>
<?php endif; ?>

<?php foreach ($search_fields as $field): ?>
<div id="fm-search-field-<?php print str_replace(' ', '-', strtolower($field->field_data['display_name'])); ?>">
  <div class="fm-field-search-input">
    <?php print $field->input_field; ?>
  </div>
</div>
<?php endforeach; ?>

<div id="fm-search-form-submit">
  <?php print $form_submit; ?>
</div>
