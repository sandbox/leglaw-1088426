//
$(document).ready(function() {
  $('a.fm-ajaxlink').each(function() {
    $(this).toggle(
      function() {
        // Turn on the loading animated gif and show expanded
        $(this).toggleClass('ajax-loading');
        $(this).toggleClass('collapsed').toggleClass('expanded');
        var thisitem = this;
        // Get response
        var fm_ajaxlink_display_detail = function(data) {
          var colspan = $('#filemaker-field-table tbody > tr:first td').length;
          thisitem.newDivId = 'detail-record-'+ thisitem.pathname.replace(/\//g,'-');
          $(thisitem).parent().parent()
            .after('<tr><td id="'+thisitem.newDivId+'" colspan="'+colspan+'" style="display:none;"></td></tr>');
          var output = data.output;
          $(output).appendTo($('#'+thisitem.newDivId));
          $('#'+thisitem.newDivId).fadeIn('slow', function() {
            // Turn off ajax loader gif and show down triangle for expanded
            $(thisitem).toggleClass('ajax-loading');
          });
        }
        $.getJSON(this.href, fm_ajaxlink_display_detail);
        return false;
      },
      function() {
        // Turn off expanded and show collapsed.
        $(this).toggleClass('expanded').toggleClass('collapsed');
        var selector = this.newDivId;
        $('#'+selector).fadeOut('slow', function(){$(this).remove();});
      }
    );
  });
});