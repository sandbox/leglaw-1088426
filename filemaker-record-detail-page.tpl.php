<?php
$path = drupal_get_path('module', 'filemaker');
drupal_add_css($path .'/css/filemaker-detail.css');
?>

<?php if ($title): ?>
<h1><?php print $title; ?></h1>
<?php endif; ?>

<div id="layout-content-area">
  <?php print $layout_content; ?>
</div>

<div id="portal-content-area">
  <?php foreach ($portal_sets_content as $set_key=>$content): ?>
  <div id="section-<?php print $set_key; ?>">
    <?php print $content; ?>
  </div>
  <?php  endforeach; ?>
</div>

<div id="fm-additional-content">
  <?php print $additional_content; ?>
</div>